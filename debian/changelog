node-mqtt-connection (4.1.0-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 08 Jan 2023 21:43:50 -0000

node-mqtt-connection (4.1.0-4) unstable; urgency=low

  [ Yadd ]
  * Team upload
  * Drop nodejs dependency
  * Mark test dependencies with <!nocheck>

  [ Ying-Chun Liu (PaulLiu) ]
  * add debian/patches/0001_fix_test_for_node-mqtt-packet-7.1.1.patch
    - Fix FTBFS (Closes: #1002255)
    - Fix autopkgtest fail (Closes: #1001726)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 01 Jan 2022 13:47:23 +0800

node-mqtt-connection (4.1.0-3) unstable; urgency=medium

  * Team upload
  * Update standards version to 4.6.0, no changes needed.
  * Add "Rules-Requires-Root: no"
  * Use dh-sequence-nodejs instead of pkg-js-tools

 -- Yadd <yadd@debian.org>  Tue, 02 Nov 2021 16:02:34 +0100

node-mqtt-connection (4.1.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on node-duplexify,
      node-mqtt-packet, node-safe-buffer, nodejs and pkg-js-tools.
    + node-mqtt-connection: Drop versioned constraint on node-duplexify,
      node-mqtt-packet, node-safe-buffer and nodejs in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 29 Aug 2021 02:19:28 +0100

node-mqtt-connection (4.1.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on node-inherits, node-through2.
    + node-mqtt-connection: Drop versioned constraint on node-inherits,
      node-through2 in Depends.

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 4.1.0

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 24 Feb 2021 04:26:22 +0800

node-mqtt-connection (4.0.0-2) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * debian/docs: Add CONTRIBUTING.md to docs
  * Re-upload source package for migating to testing.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 30 Mar 2020 00:02:39 +0800

node-mqtt-connection (4.0.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * Initial release (Closes: #950749)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 06 Feb 2020 02:30:57 +0800
